package main

import (
	"fmt"
	"github.com/BurntSushi/toml"
	"github.com/alexflint/go-arg"
	"snowops/cmd"
	settings "snowops/internal/settings"
	"strings"
)

var config settings.Settings

func main() {
	arg.MustParse(&config)
	_, _ = toml.DecodeFile("./snowops.toml", &config)

	config.ApplySettings() // this overrides toml config file with CLI flags if they exist.

	fmt.Printf("args: %+v\n", config)

	command := strings.ToLower(config.Command)

	if command == "snapshot" {
		err := cmd.SnapshotCommand(&config)

		if err != nil {
			fmt.Printf("error: %s", err.Error())
		}
	} else if command == "clone" {
		err := cmd.CloneCommand(&config)

		if err != nil {
			fmt.Printf("error: %s", err.Error())
		}
	} else if command == "apply" {
		err := cmd.ApplyCommand(&config)

		if err != nil {
			fmt.Printf("error: %s", err.Error())
		}
	} else {
		err := cmd.DiffCommand(&config)

		if err != nil {
			fmt.Printf("error: %s", err.Error())
		}
	}

}
