package db

import (
	"github.com/jmoiron/sqlx"
	"github.com/snowflakedb/gosnowflake"
	"snowops/internal/settings"
)

type DatabaseContext struct {
	DSN    string
	Config *gosnowflake.Config
	Driver string
	DB     *sqlx.DB
}

func NewDatabaseContext(config *settings.Settings, targetName string) (*DatabaseContext, error) {
	targetDb := config.Targets[targetName]
	sqlConfig, err := gosnowflake.ParseDSN(targetDb.DSN)

	if err != nil {
		return nil, err
	}

	if sqlConfig.Database == "" {
		sqlConfig.Database = targetDb.Database
	}

	fullDsn, _ := gosnowflake.DSN(sqlConfig)

	instance := &DatabaseContext{
		DSN:    fullDsn,
		Config: sqlConfig,
		Driver: "snowflake",
	}

	return instance, err
}

func (dc *DatabaseContext) InitConnection() error {
	fullDsn, _ := gosnowflake.DSN(dc.Config)

	db, err := sqlx.Connect(dc.Driver, fullDsn)

	dc.DB = db

	return err
}
