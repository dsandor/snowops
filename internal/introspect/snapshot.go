package introspect

import (
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"os"
	"path/filepath"
)

type Snapshot struct {
	Tables *[]Table
}

// WriteSnapshotFile - this function serializes the snapshot file to disk. target type is either: source or destination
func WriteSnapshotFile(targetType string, snapshotDir string, snapshot *Snapshot) error {
	filename := filepath.Join(snapshotDir, fmt.Sprintf("snapshot.%s.json", targetType))

	settingsJson, _ := json.Marshal(*snapshot)

	if err := os.WriteFile(filename, settingsJson, 0666); err != nil {
		return errors.New(fmt.Sprintf("failed writing snapshot file: %s, error: %s", filename, err.Error()))
	}

	return nil
}

// ReadSnapshotFile - reads the snapshot file to a struct
func ReadSnapshotFile(targetType string, snapshotDir string) (*Snapshot, error) {
	filename := filepath.Join(snapshotDir, fmt.Sprintf("snapshot.%s.json", targetType))

	content, err := os.ReadFile(filename)
	if err != nil {
		log.Fatal("Error when opening file: ", err)
	}

	var snapshotFile Snapshot

	// Now let's unmarshall the data into `payload`
	err = json.Unmarshal(content, &snapshotFile)

	return &snapshotFile, err
}
