package introspect

import (
	"database/sql"
	"fmt"
	"snowops/internal/db"
)

type Table struct {
	DDL      string
	Name     string `db:"TABLE_NAME"`
	Schema   string `db:"TABLE_SCHEMA"`
	Database string `db:"TABLE_CATALOG"`
	Columns  []Column
	Comment  sql.NullString `db:"COMMENT"`
}

type Column struct {
	Name                   string        `db:"COLUMN_NAME"`
	DataType               string        `db:"DATA_TYPE"`
	IsNullable             string        `db:"IS_NULLABLE"`
	IsIdentity             string        `db:"IS_IDENTITY"`
	CharacterMaximumLength sql.NullInt64 `db:"CHARACTER_MAXIMUM_LENGTH"`
	CharacterOctetLength   sql.NullInt64 `db:"CHARACTER_OCTET_LENGTH"`
	NumericPrecision       sql.NullInt32 `db:"NUMERIC_PRECISION"`
	NumericPrecisionRadix  sql.NullInt32 `db:"NUMERIC_PRECISION_RADIX"`
	NumericScale           sql.NullInt32 `db:"NUMERIC_SCALE"`
	DateTimePrecision      sql.NullInt32 `db:"DATETIME_PRECISION"`
	IdentityStart          sql.NullInt64 `db:"IDENTITY_START"`
	IdentityIncrement      sql.NullInt64 `db:"IDENTITY_INCREMENT"`
	IdentityMaximum        sql.NullInt64 `db:"IDENTITY_MAXIMUM"`
	IdentityMinimum        sql.NullInt64 `db:"IDENTITY_MINIMUM"`
	TableName              string        `db:"TABLE_NAME"`
	SchemaName             string        `db:"TABLE_SCHEMA"`
	DatabaseName           string        `db:"TABLE_CATALOG"`
}

type Tables struct {
	DbContext *db.DatabaseContext
}

const (
	TABLES_SQL_QUERY_TABLES  = `select * from information_schema.tables where table_type = 'BASE TABLE';`
	TABLES_SQL_QUERY_COLUMNS = `select * from information_schema.columns where TABLE_CATALOG = ? AND TABLE_SCHEMA = ? AND TABLE_NAME = ?;`
	TABLES_SQL_QUERY_GET_DDL = `select GET_DDL('TABLE', ?) as DDL`
)

func NewTables(dbContext *db.DatabaseContext) Tables {
	return Tables{DbContext: dbContext}
}

func (t *Tables) GetTables() (*[]Table, error) {
	var tables []Table

	if err := t.DbContext.DB.Unsafe().Select(&tables, TABLES_SQL_QUERY_TABLES); err != nil {
		return nil, err
	}

	for i, table := range tables {
		columns, err := t.GetColumnsForTable(&table)

		if err != nil {
			return nil, err
		}

		table.Columns = *columns

		ddl, err := t.GetCreateTable(table.Name, table.Schema)

		if err != nil {
			return nil, err
		}
		table.DDL = ddl

		tables[i] = table
	}

	return &tables, nil
}

func (t *Tables) GetColumnsForTable(table *Table) (*[]Column, error) {
	var columns []Column

	err := t.DbContext.DB.Unsafe().Select(&columns, TABLES_SQL_QUERY_COLUMNS, table.Database, table.Schema, table.Name)

	return &columns, err
}

func (t *Tables) GetCreateTable(table string, schema string) (string, error) {
	var row struct {
		CreateStatement string `db:"DDL"`
	}

	fqTableName := fmt.Sprintf("%s.%s", schema, table)

	if err := t.DbContext.DB.Get(&row, TABLES_SQL_QUERY_GET_DDL, fqTableName); err != nil {
		return "", err
	}

	return row.CreateStatement, nil
}
