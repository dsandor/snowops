package settings

type Settings struct {
	Command         string            `arg:"positional" help:"commands: diff, snapshot, clone, apply"`
	Targets         map[string]Target `arg:"-"`
	SourceDSN       string            `arg:"-s,--source,env:SOURCE_DSN" help:"source DSN"`
	DestDSN         string            `arg:"-d,--dest,env:DEST_DSN" help:"destination DSN"`
	ConfigFileName  string            `arg:"-c,--config" help:"path to TOML configuration file"`
	SourceWarehouse string            `arg:"--source-warehouse,env:SO_SOURCE_WAREHOUSE"`
	DestWarehouse   string            `arg:"--dest-warehouse,env:SO_DEST_WAREHOUSE"`
	SourceRole      string            `arg:"--source-role,env:SO_SOURCE_ROLE"`
	DestRole        string            `arg:"--dest-role,env:SO_DEST_ROLE"`
	SnapshotDir     string            `arg:"--snap-dir,env:SO_SNAPSHOT_DIR"`
	SnapshotFile    string            `arg:"--snap-file,env:SO_SNAPSHOT_FILE" help:"defines the snapshot file to use for the clone or migrate commands"`
	OutputDir       string            `arg:"-o,--output-dir,env:SO_OUTPUT_DIR" help:"directory to place the generated DDL"`
	Schemas         []string          `arg:"--schema" toml:"schemas"`
}

type Target struct {
	DSN       string
	Database  string
	Warehouse string
	Role      string
}

func (s *Settings) ApplySettings() {
	if s.SourceDSN != "" {
		if entry, ok := s.Targets["source"]; ok {
			entry.DSN = s.SourceDSN
			s.Targets["source"] = entry
		} else {
			s.Targets["source"] = Target{DSN: s.SourceDSN}
		}
	}

	if s.DestDSN != "" {
		if entry, ok := s.Targets["destination"]; ok {
			entry.DSN = s.DestDSN
			s.Targets["destination"] = entry
		} else {
			s.Targets["destination"] = Target{DSN: s.DestDSN}
		}
	}
}
