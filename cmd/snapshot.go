package cmd

import (
	"errors"
	"fmt"
	"os"
	"path/filepath"
	"snowops/internal/db"
	"snowops/internal/introspect"
	"snowops/internal/settings"
)

func SnapshotCommand(config *settings.Settings) error {
	fmt.Printf("config: %+v\n", config)

	if config.SnapshotDir == "" {
		return errors.New("Snapshot dir must be specified in the TOML file or on the command line.")
	}

	sourceDb, err := db.NewDatabaseContext(config, "source")

	if err != nil {
		return err
	}

	if err = sourceDb.InitConnection(); err != nil {
		return err
	}

	destDb, err := db.NewDatabaseContext(config, "destination")

	if err != nil {
		return err
	}

	if err = destDb.InitConnection(); err != nil {
		return err
	}

	if err = snapshotTarget(sourceDb, config, "source"); err != nil {
		return err
	}

	if err = snapshotTarget(destDb, config, "destination"); err != nil {
		return err
	}

	return nil
}

func snapshotTarget(db *db.DatabaseContext, config *settings.Settings, targetType string) error {
	tablesIntrospector := introspect.NewTables(db)

	targetTables, err := tablesIntrospector.GetTables()

	if err != nil {
		fmt.Printf("error intrpspecting source tables: %s\n", err.Error())
		return err
	}

	fmt.Printf("%s tables Count: %d\n", targetType, len(*targetTables))

	if err := os.MkdirAll(config.SnapshotDir, os.ModePerm); err != nil {
		return errors.New(fmt.Sprintf("failed creating snapshot directory: %s, err: %s", config.SnapshotDir, err.Error()))
	}

	// enumerate the tables and write the DDL for each to the snapshot dir.
	// ref: https://stackoverflow.com/questions/37932551/mkdir-if-not-exists-using-golang
	for _, table := range *targetTables {
		ddlFilePath := filepath.Join(config.SnapshotDir, fmt.Sprintf("%s.%s.%s.sql", table.Database, table.Schema, table.Name))
		if err := os.WriteFile(ddlFilePath, []byte(table.DDL), 0666); err != nil {
			return errors.New(fmt.Sprintf("failed writing ddl file: %s, for table: %s.%s.%s, with error: %s",
				ddlFilePath,
				table.Database,
				table.Schema,
				table.Name,
				err.Error()))
		}
	}

	snapshot := &introspect.Snapshot{Tables: targetTables}

	if err := introspect.WriteSnapshotFile(targetType, config.SnapshotDir, snapshot); err != nil {
		return err
	}

	return nil
}
