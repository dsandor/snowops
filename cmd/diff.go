package cmd

import (
	"fmt"
	"snowops/internal/db"
	"snowops/internal/introspect"
	"snowops/internal/settings"
)

func DiffCommand(config *settings.Settings) error {
	fmt.Printf("config: %+v\n", config)

	sourceDb, destDb, err := initContexts(config)

	sourceTablesIntrospector := introspect.NewTables(sourceDb)
	deestTablesIntrospector := introspect.NewTables(destDb)

	sourceTables, err := sourceTablesIntrospector.GetTables()

	if err != nil {
		fmt.Printf("error intrpspecting source tables: %s\n", err.Error())
		return err
	}

	destTables, err := deestTablesIntrospector.GetTables()

	if err != nil {
		fmt.Printf("error intrpspecting source tables: %s\n", err.Error())
		return err
	}

	fmt.Printf("source: %+v\ndest: %+v\n", sourceTables, destTables)

	return nil
}

func initContexts(config *settings.Settings) (source *db.DatabaseContext, dest *db.DatabaseContext, err error) {
	sourceDb, err := db.NewDatabaseContext(config, "source")

	if err != nil {
		return nil, nil, err
	}

	if err = sourceDb.InitConnection(); err != nil {
		return nil, nil, err
	}

	destDb, err := db.NewDatabaseContext(config, "destination")

	if err != nil {
		return nil, nil, err
	}

	if err = destDb.InitConnection(); err != nil {
		return nil, nil, err
	}

	return sourceDb, destDb, nil
}
