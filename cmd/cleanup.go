package cmd

import (
	"errors"
	"fmt"
	"path/filepath"
	"snowops/internal/db"
	"snowops/internal/settings"
)

// CleanupCommand - PHASE 3 of Migrate - this command will drop the old tables and leave the newly migrated ones.
func CleanupCommand(config *settings.Settings) error {
	if config.OutputDir == "" {
		return errors.New("output dir '-o, or --output-dir' must be specified in the TOML file or on the command line")
	}
	phase1SettingsFilePath := filepath.Join(config.OutputDir, FILENAME_PHASE1_SETTINGS)

	phase1OutputSettings, err := loadPhaseOneSettings(phase1SettingsFilePath)

	if err != nil {
		return err
	}

	// init destination database context
	destDb, err := db.NewDatabaseContext(config, "destination")

	if err != nil {
		return err
	}

	if err = destDb.InitConnection(); err != nil {
		return err
	}

	// enumerate tables in phase1 output, rename originals to OLD_TABLE_NAME_SUFFIX and NEW to Original
	for _, table := range *phase1OutputSettings.Tables {
		// drop old table.
		oldTableName := fmt.Sprintf("%s.%s%s", table.Schema, table.Name, phase1OutputSettings.OldTableSuffix)

		if err := dropOldTable(destDb, oldTableName); err != nil {
			return err
		}
	}

	return nil
}

func dropOldTable(db *db.DatabaseContext, tableName string) error {
	var sqlDropTemplate = `drop table %s;`

	if _, err := db.DB.Exec(fmt.Sprintf(sqlDropTemplate, tableName)); err != nil {
		return err
	}

	return nil
}
