package cmd

import (
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"snowops/internal/db"
	"snowops/internal/settings"
)

const (
	OLD_TABLE_NAME_SUFFIX = "_OLD"
)

// ApplyCommand - PHASE 2 of Migrate - this command will apply a clone to the database.
// This process entails enumerating all tables and views
// and renaming the existing table to _old and the new table without the _hash.
// Effectively this is Phase 2 of the apply/migrate portion of the workflow.
// The purpose is to now rename all the applicable tables that were 'cloned' to
// a temporary location and then replace all the functions and views.
func ApplyCommand(config *settings.Settings) error {
	if config.OutputDir == "" {
		return errors.New("output dir '-o, or --output-dir' must be specified in the TOML file or on the command line")
	}
	phase1SettingsFilePath := filepath.Join(config.OutputDir, FILENAME_PHASE1_SETTINGS)

	phase1OutputSettings, err := loadPhaseOneSettings(phase1SettingsFilePath)

	if err != nil {
		return err
	}

	// init destination database context
	destDb, err := db.NewDatabaseContext(config, "destination")

	if err != nil {
		return err
	}

	if err = destDb.InitConnection(); err != nil {
		return err
	}

	// enumerate tables in phase1 output, rename originals to OLD_TABLE_NAME_SUFFIX and NEW to Original
	for _, table := range *phase1OutputSettings.Tables {
		// Rename existing table to old.
		fromTableName := fmt.Sprintf("%s.%s", table.Schema, table.Name)
		toTableName := fmt.Sprintf("%s.%s%s", table.Schema, table.Name, OLD_TABLE_NAME_SUFFIX)

		if err := renameTable(destDb, fromTableName, toTableName); err != nil {
			return err
		}

		// Rename new table to replace old existing.
		fromTableName = fmt.Sprintf("%s.%s%s", table.Schema, table.Name, phase1OutputSettings.Hash)
		toTableName = fmt.Sprintf("%s.%s", table.Schema, table.Name)

		if err := renameTable(destDb, fromTableName, toTableName); err != nil {
			return err
		}
	}

	phase1OutputSettings.OldTableSuffix = OLD_TABLE_NAME_SUFFIX

	if err := writeCloneSettings(config.OutputDir, phase1OutputSettings, FILENAME_PHASE1_SETTINGS); err != nil {
		return err
	}

	return nil
}

// loadPhaseOneSettings - loads the settings from the phase 1 step (clone) into the struct.
func loadPhaseOneSettings(filename string) (*CloneSettings, error) {
	content, err := os.ReadFile(filename)
	if err != nil {
		log.Fatal("Error when opening file: ", err)
	}

	var cloneSettings CloneSettings

	// Now let's unmarshall the data into `payload`
	err = json.Unmarshal(content, &cloneSettings)

	return &cloneSettings, err
}

func renameTable(db *db.DatabaseContext, fromTableName string, toTableName string) error {
	var sqlRenameTemplate = `alter table %s rename to %s;`

	if _, err := db.DB.Exec(fmt.Sprintf(sqlRenameTemplate, fromTableName, toTableName)); err != nil {
		return err
	}

	return nil
}
