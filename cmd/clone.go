package cmd

import (
	"encoding/json"
	"errors"
	"fmt"
	"golang.org/x/exp/slices"
	"log"
	"os"
	"path/filepath"
	"snowops/internal/db"
	"snowops/internal/introspect"
	"snowops/internal/settings"
	"strings"
)

type CloneSettings struct {
	Hash           string
	OldTableSuffix string
	OutputDir      string
	Tables         *[]introspect.Table
	// TODO: Need Views
}

const (
	FILENAME_PHASE1_SETTINGS = "apply-phase1-settings.json"
)

// CloneCommand - PHASE 1 of Migrate - the clone command will take a snapshot and create all the
// defined objects side by side with a hash suffix. Data from the original tables will
// be copied into the new table.  This is the second phase in the migration workflow.
func CloneCommand(config *settings.Settings) error {
	if config.SnapshotFile == "" {
		return errors.New("snapshot file '--snap-file' must be specified in the TOML file or on the command line")
	}

	if config.OutputDir == "" {
		return errors.New("output dir '-o, or --output-dir' must be specified in the TOML file or on the command line")
	}

	if err := os.MkdirAll(config.OutputDir, os.ModePerm); err != nil {
		return errors.New(fmt.Sprintf("failed creating output directory: %s, err: %s", config.OutputDir, err.Error()))
	}

	// init destination database context
	destDb, err := db.NewDatabaseContext(config, "destination")

	if err != nil {
		return err
	}

	if err = destDb.InitConnection(); err != nil {
		return err
	}

	// Workflow:
	// Load in the tables.DATABASE.json snapshot file.
	// enumerate the tables:
	// 	for each file, change table name to include suffix.
	//	execute the DDL to create the new table with suffix
	//  and copy from the existing table (CTAS) ref: https://docs.snowflake.com/en/sql-reference/sql/create-table
	//  optionally take a CTAS SELECT statement from a `data-conversion-script` file.  This allows a user
	//  to define a data conversion select statement that properly imports the data into the new table.

	tables, err := loadTablesSnapshot(config.SnapshotFile)

	if err != nil {
		return errors.New(fmt.Sprintf("failed loading snapshot file: %s, error: %s", config.SnapshotFile, err.Error()))
	}

	hash := "_COPY"
	copyToSqlTemplate := `SELECT * FROM %s.%s`

	var cloneTableNames []string

	for _, table := range *tables {
		copyToSql := fmt.Sprintf(copyToSqlTemplate, table.Schema, table.Name)

		sql, cloneTableName := assembleCTAS(table, copyToSql, hash)

		// keep a list of the clone tables we need to drop when the process completes.
		cloneTableNames = append(cloneTableNames, *cloneTableName)

		// execute DDL to create clone table.
		// -or- write the DDL to files.
		if err := writeTableOutput(destDb, table, config.OutputDir, sql, false); err != nil {
			return err
		}
	}

	cleanupScriptName := "_cleanup.sql"
	cleanupScript := createCleanupScript(cloneTableNames)

	if err := writeCleanupOutput(config.OutputDir, cleanupScript, cleanupScriptName); err != nil {
		return err
	}

	cloneSettings := &CloneSettings{
		Hash:      hash,
		OutputDir: config.OutputDir,
		Tables:    tables,
	}

	if err := writeCloneSettings(config.OutputDir, cloneSettings, FILENAME_PHASE1_SETTINGS); err != nil {
		return err
	}

	return nil
}

// writeTableOutput - writes the table SQL/DDL to a file. If the dryRun boolean is set to true only the file is written
// however, if the dryRun boolean is false then the SQL is applied to the destination database as well.
func writeTableOutput(db *db.DatabaseContext, table introspect.Table, dir string, sql *string, dryRun bool) error {
	cloneFilePath := filepath.Join(dir, fmt.Sprintf("clone_table.%s.%s.sql", table.Schema, table.Name))

	if err := os.WriteFile(cloneFilePath, []byte(*sql), 0666); err != nil {
		return errors.New(fmt.Sprintf("failed writing clone table file: %s, error: %s", cloneFilePath, err.Error()))
	}

	if dryRun {
		return nil
	}

	db.Config.Schema = table.Schema

	if _, err := db.DB.Exec(*sql); err != nil {
		return err
	}

	return nil
}

func writeCleanupOutput(dir string, sql *string, cleanupScriptName string) error {

	cloneFilePath := filepath.Join(dir, cleanupScriptName)

	if err := os.WriteFile(cloneFilePath, []byte(*sql), 0666); err != nil {
		return errors.New(fmt.Sprintf("failed writing clone table file: %s, error: %s", cloneFilePath, err.Error()))
	}

	return nil
}

func writeCloneSettings(dir string, cloneSettings *CloneSettings, cloneSettingsName string) error {

	cloneFilePath := filepath.Join(dir, cloneSettingsName)

	settingsJson, _ := json.Marshal(*cloneSettings)

	if err := os.WriteFile(cloneFilePath, settingsJson, 0666); err != nil {
		return errors.New(fmt.Sprintf("failed writing clone settings file: %s, error: %s", cloneSettingsName, err.Error()))
	}

	return nil
}

// loadTablesSnapshot - loads the snapshot file from the snapshot directory and unmarshalls it into
// a slice of table structs.
func loadTablesSnapshot(filename string) (*[]introspect.Table, error) {
	content, err := os.ReadFile(filename)
	if err != nil {
		log.Fatal("Error when opening file: ", err)
	}

	var tables []introspect.Table

	// Now let's unmarshall the data into `payload`
	err = json.Unmarshal(content, &tables)

	return &tables, err
}

// assembleCTAS - generates two strings, the first is the DDL to create the
// clone table and the second is the name of the clone table.
func assembleCTAS(table introspect.Table, statement string, hash string) (*string, *string) {
	var sql string

	// first, find the table name in the DDL.
	// replace the name with the name + hash.
	// remove the end statement `;` and insert
	// the statement followed by the `;`.

	parts := strings.Split(table.DDL, " ")

	nameLocation := slices.Index[string](parts, table.Name)

	if nameLocation < 1 {
		// table name not found.
		return nil, nil
	}

	shadowTableName := fmt.Sprintf("%s.%s%s", table.Schema, table.Name, hash)
	parts[nameLocation] = shadowTableName

	lastPart := strings.ReplaceAll(parts[len(parts)-1], ";", " AS ")

	parts[len(parts)-1] = lastPart

	sql = strings.Join(parts, " ") + statement + ";"

	return &sql, &shadowTableName
}

// createCleanupScript - creates a script that drops all the cloned tables.
func createCleanupScript(cloneTableNames []string) *string {
	var cleanupScriptSql string

	tableDropTemplate := `DROP TABLE %s;`

	var dropStatements []string

	for _, tableName := range cloneTableNames {
		dropStatements = append(dropStatements, fmt.Sprintf(tableDropTemplate, tableName))
	}

	cleanupScriptSql = strings.Join(dropStatements, "\n")

	return &cleanupScriptSql
}
