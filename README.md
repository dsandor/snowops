
## Steps

1. `snowops snapshot`
2. `snowops clone`
3. `snowops apply`



```
DB Dev Workflow

Clone Prod
Make changes
Create branch of db repo
take snapshot of modified clone
Create merge request


MR triggers:
	a clone of prod
	apply MR to prod clone (MR snap applied to 'mr' prod clone)
	test
	Merge MR to main

Merge to main triggers:
	snapshot apply to prod


```